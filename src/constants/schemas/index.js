import business from './business';
import value from './value';
import lean from './lean';

export default { business, value, lean };
